import pymssql
import re

import boto3
import redis


# get asg name (ms4 + blue|green)
def get_ms4_group_name():
    client = boto3.client('autoscaling')
    response = client.describe_auto_scaling_groups()
    for group in response['AutoScalingGroups']:
        if re.match(r'ms4', group['AutoScalingGroupName']):
            return group['AutoScalingGroupName']


def get_ms4_group():
    ms4_name = get_ms4_group_name()
    client = boto3.client('autoscaling')
    response = client.describe_auto_scaling_groups(
        AutoScalingGroupNames=[
            ms4_name
        ])
    current_capacity = response['AutoScalingGroups'][0]['DesiredCapacity']
    return current_capacity


def get_ms4_redis_queues():
    redis_host = ''
    redis_port = 6380
    queue_name = ''
    r = redis.StrictRedis(host=redis_host, port=redis_port)
    return r.llen(queue_name)


def get_mssql_broadcasts_count():
    query = """"""  # query for getting number of broadcasts
    server = ''  # mssql server name or ip
    user = ''  # mssql username
    password = ''  # mssql password
    db = ''  # db name
    conn = pymssql.connect(server, user, password, db)
    cursor = conn.cursor()
    cursor.execute(query)
    contacts_count = cursor.fetchone()[1]
    conn.close()
    return contacts_count


def scale_up():
    # TODO


def scale_down():
    # TODO


def print_info():
    print('MSSql: ' + str(get_mssql_broadcasts_count()))
    print('Redis: ' + str(get_ms4_redis_queues()))
    print('MS4 current capacity: ' + str(get_ms4_group()))


def check_queues():
    redis_count = get_ms4_redis_queues()
    if get_mssql_broadcasts_count() != None:
        mssql_count = get_mssql_broadcasts_count()
    else:
        mssql_count = 0
    if redis_count > 190000 and mssql_count > redis_count:
        scale_up()
    elif redis_count < 190000 and mssql_count < redis_count:
        scale_down()
    else:
        print('Nothing to do')


print_info()
