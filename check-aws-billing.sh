#!/usr/bin/env bash

PREV_DATE=$(date -d yesterday +%Y-%m-%d)
CUR_DATE=$(date -d today +%Y-%m-%d)
PREV_WEEK=$(date -d "today -7 days" +%Y-%m-%d)

AMOUNT=$(sudo aws --region us-east-1 ce get-cost-and-usage --time-period Start=${PREV_DATE},End=${CUR_DATE} --metrics BlendedCost  --granularity DAILY | jq '.ResultsByTime[].Total.BlendedCost.Amount' | sed 's/\"//g'| cut -d "." -f1)
PREV_AMOUNTS=$(sudo aws --region us-east-1 ce get-cost-and-usage --time-period Start=${PREV_WEEK},End=${CUR_DATE} --metrics BlendedCost  --granularity DAILY | jq '.ResultsByTime[].Total.BlendedCost.Amount' | sed 's/\"//g' | cut -d "." -f1)
S=0
for i in ${PREV_AMOUNTS[@]}; do let "S += $i"; done
let "AVERAGE_BILL = $S / 7"
let "MAX_AMOUNT= $AVERAGE_BILL + ($AVERAGE_BILL / 100 * 15)"

if [ $AMOUNT -gt $MAX_AMOUNT ]; then
  RESULT="Amount $AMOUNT greater the MAX_AMOUNT"
else
  RESULT=$AMOUNT
fi
