#!/usr/bin/env bash

docker run --rm \
       --name=selenium \
       -v `pwd`:/app \
       -d -P -p 5900:5900 -p 4444:4444 \
       -v /dev/shm:/dev/shm \
       selenium/standalone-chrome-debug:3.141.59-gold

docker run --rm  \
        --net=host \
        --name=tests \
        -v `pwd`:/app \
        -v ~/.m2:/var/maven/.m2 \
        -e MAVEN_CONFIG=/var/maven/.m2 \
        -w /app \
        -v /etc/group:/etc/group:ro -v /etc/passwd:/etc/passwd:ro \
        -u $( id -u $USER ):$( id -g $USER ) \
        -e runOn=REMOTE \
        -e host=http://localhost \
        -e port=4444 \
        maven:3-jdk-8-alpine \
        mvn -Duser.home=/var/maven \
        -DrunOn=REMOTE \
        -Dhost=localhost \
        -Dport=4444 $1\
        clean test
}

rm -f ${failed_tests_file}
docker rm -f $(docker ps -aq)
run_tests $ext_opts
cat logFile.log | grep FAILED | cut -d . -f6 | awk '{print $1"#"$3}' >> $failed_tests_file
if [  -f ${failed_tests_file} ]; then
  for line in $(cat $failed_tests_file); do
  run_tests "-Dtest=${line}"
  done
fi