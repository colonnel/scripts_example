#!/usr/bin/env python3
# for alerting of 503 errors
import json
import os

from elasticsearch import Elasticsearch

es = Elasticsearch([{'host': '', 'port': 9200}])


# Build request to elasticsearch
def build_request():
    return json.dumps({
        "size": 0,
        "aggs": {
            "atby_responseId": {
                "filter": {
                    "bool": {
                        "must": [
                            {
                                "range": {
                                    "@timestamp": {
                                        "gte": "now-1m/m",
                                        "lte": "now/m",
                                        "format": "yyyy-MM-dd HH:mm:ss"
                                    }
                                }
                            }
                        ]
                    }
                },
                "aggs": {
                    "group_by_responseId": {
                        "terms": {
                            "field": "response.keyword",
                            "size": 50
                        }
                    }
                }
            }
        }
    })


def get_error_count():
    resp = es.search(index='logstash-*', body=build_request())
    codes = resp['aggregations']['atby_responseId']['group_by_responseId']['buckets']
    for code in codes:
        if code['key'] == '503':
            return (code['doc_count'])
        # else:
        #     return 0


def send_to_zabbix():
    if get_error_count() != None:
        result = str(get_error_count())
    else:
        result = '0'
    cmd = "/usr/bin/zabbix_sender -z """ -p 10051 -s \"Zabbix server\" -k '' -o " + result + ""
    os.system(cmd)


send_to_zabbix()
