
#!/usr/bin/env bash
ZABBIX_IP=""
INSTANCES="$(aws ec2 describe-instance-status --filters Name=instance-status.status,Values=impaired | jq '.InstanceStatuses[].InstanceId' | sed 's/["]//g')"
if [ -z ${INSTANCES} ]; then
  RESULT=0
else
  RESULT=$(aws ec2 describe-instances --instance-ids ${INSTANCES} | jq '.Reservations[].Instances[].Tags[] | select(.Key=="Name") | .Value' | sed 's/["]//g')
fi
echo ${RESULT}
/usr/bin/zabbix_sender -z ${ZABBIX_IP} -p 10051 -s "Zabbix server"  -k 'instance.health' -o "${RESULT}"