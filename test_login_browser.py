#!/usr/bin/env python3
from selenium import webdriver
import time

start_url = 'https://esputnik.com/es/login/'
next_url = 'https://esputnik.com/dashboard-ui/#/'
login = ''
password = ''
chromedriver_path = '/opt/chromdriver/chromedriver'
options = webdriver.ChromeOptions()
options.add_argument('--headless')
browser = webdriver.Chrome(chromedriver_path, chrome_options=options)
browser.get(start_url)
email_form = browser.find_element_by_name('username')
password_form = browser.find_element_by_name('password')
signin_button = browser.find_element_by_css_selector('button[type="submit"]')
email_form.send_keys(login)
password_form.send_keys(password)
signin_button.click()
time.sleep(20)
titles_list = []
try:
    titles_list.append(browser.find_element_by_class_name('es-dashboard-contacts'))
except Exception as e:
    print()
try:
    titles_list.append(browser.find_element_by_class_name('es-dashboard-messages'))
except Exception:
    print()
browser.close()
if len(titles_list) < 2:
    print(0)
else:
    print(1)