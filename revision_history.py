# script to save deploy history to google doc
import datetime
import sys
import xml.etree.cElementTree as ET
import gspread
import urllib
from oauth2client.service_account import ServiceAccountCredentials

doc_name = '' # goodle doc name
credentials_file = '' # path to json cred file
url = sys.argv[1]
job_name = sys.argv[2]
correct_build_url = urllib.parse.unquote_plus(url)
correct_build_url = correct_build_url.replace('http://jenkins:8080/job', '/var/lib/jenkins/jobs') # change build url
path_to_xml = correct_build_url.replace(job_name, job_name + '/builds') + "build.xml"
CUR_REV_COL = 2
PREV_REV_COL = 3
PART_COL_ON_BIG = 6
CUR_REV_COL_ON_BIG = 3
PREV_REV_COL_ON_BIG = 5


def init_spreadsheet(cred_file):
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name(cred_file, scope)
    gc = gspread.authorize(credentials)
    return gc


def make_record(file):
    tree = ET.parse(file)
    root = tree.getroot()
    date = str("{:%Y-%m-%d  %H:%M}".format(datetime.datetime.today()))
    try:
        service_name = sys.argv[3]
    except Exception:
        service_name = 'undefined'
    try:
        revision = sys.argv[4]
    except Exception:
        revision = 'latest'
    status = root.find('result').text
    try:
        part = sys.argv[5]
    except Exception:
        part = ''
    user_name = root.find('.//actions/hudson.model.CauseAction/causeBag/entry/hudson.model.Cause_-UserIdCause/').text
    prev_rev = get_prev_rev(service_name, part)
    return [date, service_name, revision, status, prev_rev, part, user_name]


def insert_record():
    doc = init_spreadsheet(credentials_file)
    worksheet = doc.open(doc_name).worksheet('revisions')
    record = make_record(path_to_xml)
    change_rev_from_prev_to_cur_on_short(record)
    print(record)
    worksheet.append_row(record)


def get_prev_rev(service_name, part):
    doc = init_spreadsheet(credentials_file)
    worksheet = doc.open(doc_name).worksheet('revisions')
    service_name_records = worksheet.findall(service_name)
    for cell in reversed(service_name_records):
        print(worksheet.cell(cell.row, PART_COL_ON_BIG).value)
        if worksheet.cell(cell.row, PART_COL_ON_BIG).value == part:
            return worksheet.cell(cell.row, CUR_REV_COL_ON_BIG).value
            break
        else:
            return ''


def change_rev_from_prev_to_cur_on_short(record):
    doc = init_spreadsheet(credentials_file)
    short_worksheet = doc.open(doc_name).worksheet('short_table')
    service_name = record[1]
    service_name_records = short_worksheet.findall(service_name)
    if len(service_name_records) == 0:
        short_worksheet.append_row([record[1], record[2], record[4]])
    else:
        try:
            cell_name = service_name_records[-1]
        except Exception:
            return ''
        cur_rev = short_worksheet.cell(cell_name.row, CUR_REV_COL).value
        if record[2] != cur_rev:
            short_worksheet.update_cell(cell_name.row, 3, cur_rev)
            short_worksheet.update_cell(cell_name.row, CUR_REV_COL, record[2])


insert_record()
